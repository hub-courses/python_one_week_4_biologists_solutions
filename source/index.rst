.. E2I2B python solutions documentation master file, created by
   sphinx-quickstart on Sat Jul 12 05:10:50 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python Solutions's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Data_Types
   Collection_Data_Types
   Control_Flow_Statements
   Dive_into_Functions
   Input_Output
   Modules_and_Packages
   Object_Oriented_Programming


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
