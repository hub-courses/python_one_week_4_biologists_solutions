import csv


def parse_gene_file(path):
    genes = set()
    with open(path, 'r') as input:
        reader = csv.reader(input, quotechar='"')
        for row in reader:
            id_, symbol, _, name, entrez, uniprot, *_ = row
            genes.add((symbol, name, entrez, uniprot))
    return genes


if __name__ == '__main__':
    exp1 = parse_gene_file('exp1.csv')
    exp2 = parse_gene_file('exp2.csv')

    exp1_symbol = {item[-1] for item in exp1}
    exp2_symbol = {item[-1] for item in exp2}

    spe = exp1_symbol - exp2_symbol
    with open('exp1_specific.csv', 'w') as out:
        writer = csv.writer(out)
        for row in exp1:
            if row[-1] in spe:
                writer.writerow(row)