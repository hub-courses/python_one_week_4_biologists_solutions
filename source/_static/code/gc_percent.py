
def gc_percent(seq):
    """
    :param seq: The nucleic sequence to compute
    :type seq: string
    :return: the percent of GC in the sequence
    :rtype: float
    """
    seq = seq.upper()
    gc_pc = float(seq.count('G') + seq.count('C')) / float(len(seq))
    return gc_pc
