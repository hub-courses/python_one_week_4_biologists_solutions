

def rev_comp(seq):
    """
    return the reverse complement of seq
    the case is respect but if the sequence mix upper and lower case the function will failed
    """
    reverse = seq[::-1]
    direct = 'acgtATCG'
    comp = 'tgcaTGCA'
    table = str.maketrans(direct, comp)
    rev_comp = reverse.translate(table)
    return rev_comp