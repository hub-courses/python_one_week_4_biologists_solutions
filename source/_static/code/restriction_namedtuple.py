##################################################
# Solution using nametuple instead classic tuple #
# see how the code is more readable              #
##################################################

import collections
RestrictEnzyme = collections.namedtuple("RestrictEnzyme", ("name", "comment", "sequence", "cut", "end"))

ecor1 = RestrictEnzyme("EcoRI", "Ecoli restriction enzime I", "gaattc", 1, "sticky")
ecor5 = RestrictEnzyme("EcoRV", "Ecoli restriction enzime V", "gatatc", 3, "blunt")
bamh1 = RestrictEnzyme("BamHI", "type II restriction endonuclease from Bacillus amyloliquefaciens ", "ggatcc", 1, "sticky")
hind3 = RestrictEnzyme("HindIII", "type II site-specific nuclease from Haemophilus influenzae", "aagctt", 1 , "sticky")
taq1 = RestrictEnzyme("TaqI", "Thermus aquaticus", "tcga", 1 , "sticky")
not1 = RestrictEnzyme("NotI", "Nocardia otitidis", "gcggccgc", 2 , "sticky")
sau3a1 = RestrictEnzyme("Sau3aI", "Staphylococcus aureus", "gatc", 0 , "sticky")
hae3 = RestrictEnzyme("HaeIII", "Haemophilus aegyptius", "ggcc", 2 , "blunt")
sma1 =  RestrictEnzyme("SmaI", "Serratia marcescens", "cccggg", 3 , "blunt")


from operator import itemgetter



def one_enz_one_binding_site(dna, enzyme):
    """
    :return: the first position of enzyme binding site in dna or None if there is not
    :rtype: int or None
    """
    print("one_enz_binding_one_site", dna, enzyme)
    pos = dna.find(enzyme.sequence)
    print("one_enz_binding_one_site", pos)
    if pos != -1:
        return pos

        
def one_enz_all_binding_sites(dna, enzyme):
    """
    :param dna: the dna sequence to search enzyme binding sites
    :type dna: str
    :param enzyme: the enzyme to looking for
    :type enzyme:  a namedtuple RestrictEnzyme
    :return: all positions of enzyme binding sites in dna
    :rtype: list of int
    """
    positions = []
    pos = dna.find(enzyme.sequence)
    while pos != -1:
        positions.append(pos)
        pos = dna.find(enzyme.sequence, pos + 1)
    return positions


def one_enz_all_binding_sites2(dna, enzyme):
    """
    :param dna: the dna sequence to search enzyme binding sites
    :type dna: str
    :param enzyme: the enzyme to looking for
    :type enzyme:  a namedtuple RestrictEnzyme
    :return: all positions of enzyme binding sites in dna
    :rtype: list of int
    """
    positions = []
    pos = dna.find(enzyme.sequence)
    while pos != -1:
        if positions:
            positions.append(pos)
        else:
            positions = pos + positions[-1]
        new_seq = dna[pos + 1:]
        pos = new_seq.find(enzyme.sequence)
        pos = pos
    return positions


def binding_sites(dna, enzymes):
    """
    return all positions of all enzymes binding sites present in dna
    sort by the increasing position.

    :param dna: the dna sequence to search enzyme binding sites
    :type dna: str
    :param enzyme: the enzyme to looking for
    :type enzyme:  a namedtuple RestrictEnzyme
    :return: all positions of each enzyme binding sites in dna
    :rtype: list of int
    """
    positions = []
    for enzyme in enzymes:
        pos = one_enz_all_binding_sites(dna, enzyme)
        pos = [(enzyme.name, pos) for pos in pos]
        positions.extend(pos)
    positions.sort(key=itemgetter(1))
    return positions 

