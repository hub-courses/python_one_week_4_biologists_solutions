import math

def vol_of_sphere(radius):
    """
    compute the volume of sphere of a given radius
    work only in python3
    """
    
    vol = 4 / 3 * math.pi * pow(radius, 3)
    return vol


def vol_of_sphere_python2(radius):
    """
    compute the volume of sphere of a given radius
    work with python2 and 3
    """

    vol = float(4) / float(3) * float(math.pi) * pow(radius, 3)
    return vol
