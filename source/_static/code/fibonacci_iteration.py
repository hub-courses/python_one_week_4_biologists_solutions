
def fibonacci(n):
    """
    compute the suite of fibonacci for n element and return it
    for instance ::

        fibonacci(7)
        [0, 1, 1, 2, 3, 5, 8, 13, 21]

    :param int n: the nth elemnt of the fibonacci suite
    :return: the nfirst elt of the fibonacci suite
    :rtype: list of int
    """
    fib_suite = []
    i = 0
    while i <= n:
        if i == 0:
            fib_suite.append(0)
        elif i == 1:
            fib_suite.append(1)
        else: 
            res = fib_suite[-1] + fib_suite[-2]
            fib_suite.append(res)
        i += 1
    return fib_suite

print(', '.join([str(i) for i in fibonacci(10)]))


def fibonacci_2(n):
    """
    compute the fibonacci of the elt n
    :param int n:
    :return: the fibonacci of the elt n
    :rtype: int
    """
    a = 0
    b = 1
    for i in range(n):
        a, b = b, a + b
    return b

