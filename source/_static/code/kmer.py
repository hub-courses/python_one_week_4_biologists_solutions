
def get_kmer_occurences(seq, kmer_len):
    """
    return a list of tuple
    each tuple contains a kmers present in seq and its occurence
    """
    # Counter dictionary (keys will be kmers, values will be counts)
    kmers = {}
    stop = len(seq) - kmer_len
    for i in range(stop + 1):
        # Extract kmer starting at position i
        kmer = seq[i: i + kmer_len]
        # Increment the count for kmer
        # or create the entry the first time this kmer is seen
        # using default value 0, plus 1
        kmers[kmer] = kmers.get(kmer, 0) + 1
    # pairs (kmer, count)
    return kmers.items()
